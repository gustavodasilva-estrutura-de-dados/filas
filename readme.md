# Atividade

Função push é utilizada para adicionar os valores na minha pilha 
```js
    this.push = function(){
        
        const value = document.getElementById("valorAddPilha").value;

        if(!value){
            alert("Informe um valor para adicionar !");
            return false;
        }

        itensPilha.push(value);
        this.createItemStack(value);

        //limpando o input
        document.getElementById("valorAddPilha").value = "";

        this.updateDataStack();

    }
```

Função getElementById: Retorna o valor pilha passado no editor
```js    
    document.getElementById("valorAddPilha").value;
    }
```

Função createItemStack: Cria os atibutos e elementos da pilha de Itens 
```js    

    this.createItemStack = function(value){

        //DOM
        let elementoPilha = document.getElementById("pilha");
        
            const divItem = document.createElement("div");
            const textElement = document.createTextNode(`${value}`);

            const attributeClass = document.createAttribute("class");
            const attributeId = document.createAttribute("id");

            attributeClass.value = "item";
            attributeId.value = itensPilha.length;

            divItem.appendChild(textElement);
            divItem.setAttributeNode(attributeClass);
            divItem.setAttributeNode(attributeId);

        //associando a div criada ao elemento pilha do html
        elementoPilha.prepend(divItem);
    }
```
Função getElementById: Retorna o valor pilha passado no editor

Função pop remove o último elemento adicionado na pilha
```js    
    this.pop = function(){

        //Remove da tela
        const lastIndexStack = document.getElementById(itensPilha.length);
        lastIndexStack.remove();

        //Remove do vetor
        itensPilha.pop();
        this.updateDataStack();

    }
```

Função clean limpa todos os elementos que estão na pilha 
```js    
     this.clean = function(){

        //revover os child da pilha
        document.getElementById("pilha").innerHTML = "";
        itensPilha = [];
        this.updateDataStack();

    }
```

Função clean limpa todos os elementos que estão na pilha 
```js    
     this.clean = function(){

        //revover os child da pilha
        document.getElementById("pilha").innerHTML = "";
        itensPilha = [];
        this.updateDataStack();

    }
```
Função updateDataStack Atualiza os dados da pilha de itens 
```js    
    this.updateDataStack = function(){

        document.getElementById("tamanhoPilha").innerHTML = itensPilha.length;
        
        let topoPilha = itensPilha[itensPilha.length - 1];

        //validar para nao ficar undefined
        !topoPilha && (topoPilha = "");
        
        document.getElementById("topoPilha").innerHTML = topoPilha;
    }
```
